# Pew!
A multi-threaded raytracer following https://raytracing.github.io/

## Dependencies
```
sdl2
sdl2_ttf
```

## Build
```sh
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build . --config Release
```
## Run
```sh
# make sure that the asset folder is on the same path
./build/src/pew/pew
```

## Controls
```
<SPACE>     Pause rendering
u           Show final render
i           Show materials as colors
s           Save render in current state at ./shot.png
```


## WASM
Wasm is currently wip on the `wasm` branch.  
Click here for a live demo:
https://about.evysgarden.org/raytrace/main.html

## Screenshots

##### A complete scene
![Rendered scene](./assets/scene.png)

##### [Viking room](https://sketchfab.com/3d-models/viking-room-a49f1b8e4f5c4ecf9e1fe7d81915ad38) by [nigelgoh](https://sketchfab.com/nigelgoh) ([CC BY 4.0](https://web.archive.org/web/20200428202538/https://sketchfab.com/3d-models/viking-room-a49f1b8e4f5c4ecf9e1fe7d81915ad38))
![Viking room](./assets/obj.png)

##### Rendered out image
![another shot](./assets/shot.png)

##### Materials as colors
![Materials as colors](./assets/materials.png)

##### Screenshot of a scene being rendered
![Rendering a scene](./assets/rendering.png)
