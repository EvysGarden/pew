// #define PEW_DISABLE_GAMMA_CORRECTION
// #define PEW_USE_NAIVE_VECTORS

#include <pew/camera/depthCamera.hpp>
#include <pew/camera/hitCamera.hpp>
#include <pew/hittable/collection.hpp>
#include <pew/hittable/mesh.hpp>
#include <pew/hittable/sphere.hpp>
#include <pew/hittable/triangle.hpp>
#include <pew/material/dielectric.hpp>
#include <pew/material/lambertian.hpp>
#include <pew/material/metallic.hpp>
#include <pew/material/normal.hpp>
#include <pew/material/opaque.hpp>
#include <pew/material/texture.hpp>
#include <pew/random/random.hpp>
#include <pew/scene/scene.hpp>
#include <pew/texture/holder.hpp>

using namespace pew;
using namespace pew::material;

void
populateScene(Scene& scene) {
    auto matGround = scene.addMaterial<Lambertian>(vec3(0.5));
    scene.add<Sphere>(vec3(0, -1000, 0), 1000, matGround);

    for (int a = -11; a < 11; ++a) {
        for (int b = -11; b < 11; ++b) {
            auto choose = PCG_T::randomDouble();
            vec3 center(a + 0.9 * PCG_T::randomDouble(), 0.2, b + 0.9 * PCG_T::randomDouble());

            if ((center - vec3(4, 0.2, 0)).length() > 0.9) {
                if (choose < 0.8) {
                    auto albedo = vec3::random() * vec3::random();
                    auto mat    = scene.addMaterial<Lambertian>(albedo);
                    scene.add<Sphere>(center, 0.2, mat);
                } else if (choose < 0.95) {
                    auto albedo = vec3::random();
                    auto fuzz   = PCG_T::randomDouble();
                    auto mat    = scene.addMaterial<Metallic>(albedo, fuzz);
                    scene.add<Sphere>(center, 0.2, mat);
                } else {
                    auto mat = scene.addMaterial<Dialetric>(1.5);
                    scene.add<Sphere>(center, 0.2, mat);
                }
            }
        }
    }

    {
        auto mat = scene.addMaterial<Dialetric>(1.5);
        scene.add<Sphere>(vec3(0, 1, 0), 1.0, mat);
    }
    {
        auto mat = scene.addMaterial<Lambertian>(vec3(0.4, 0.2, 0.1));
        scene.add<Sphere>(vec3(-4, 1, 0), 1.0, mat);
    }
    {
        auto mat = scene.addMaterial<Metallic>(vec3(0.7, 0.6, 0.5), 0.0);
        scene.add<Sphere>(vec3(4, 1, 0), 1.0, mat);
    }
}

void
triangleScene(Scene& scene) {
    auto matGround   = scene.addMaterial<Lambertian>(vec3(0.9, 0.5, 0.6));
    auto matCenter   = scene.addMaterial<Lambertian>(vec3(0.1, 0.2, 0.5));
    auto matLeft     = scene.addMaterial<Dialetric>(1.5);
    auto matRight    = scene.addMaterial<Metallic>(vec3(0.8, 0.6, 0.2), 0.0);
    auto matTriangle = scene.addMaterial<Metallic>(vec3(0.5), 0.0);
    // auto matTriangle = scene.addMaterial<Normal>();
    // scene.add<Sphere>(vec3(0, -100.5, -1), 100, matGround);
    scene.add<Sphere>(vec3(0, 1.1, 0), 0.125, matCenter);
    scene.add<Sphere>(vec3(.5, 1.1, -.1), 0.125, matLeft);
    // scene.add<Sphere>(vec3(-1, 0, -1), -0.45, matLeft);
    scene.add<Sphere>(vec3(.5, 1.3, -.6), 0.125, matRight);

    Vertex v0 { vec3(-.25, 1.2, -.5), vec3(0, 0, 1), { 0, 0 } };
    Vertex v1 { vec3(0, 1.7, -.5), vec3(0, 0, 1), { 0, 0 } };
    Vertex v2 { vec3(.25, 1.2, -.5), vec3(0, 0, 1), { 0, 0 } };
    scene.add<Triangle>(v0, v1, v2, matTriangle);
}

void
vikingScene(Scene& scene) {
    // auto material = scene.addMaterial<Metallic>(vec3(0.5), 0.0);
    // TextureHolder texture{"./assets/viking_room.png"};
    // auto material = scene.addMaterial<Normal>();

    auto texture  = scene.addTexture("./assets/viking_room.png");
    auto material = scene.addMaterial<Texture>(texture);

    Mesh mesh("./assets/viking_room.obj", vec3(0, 1, -.5));

    scene.add<Collection>(mesh.collect(material));
}

int
main() {
    constexpr auto aspect   = 16.0 / 9.0;
    constexpr auto width    = 1920;
    constexpr auto height   = static_cast<int>(width / aspect);
    constexpr auto lookFrom = vec3(0, 2, 1);
    constexpr auto lookAt   = vec3(0, 1, -1);

    Scene scene { { width, height } };
    // scene.getRenderPass().setMaxDepth(50).setSampleRate(100).setCamera<DepthCamera>(
    //     lookFrom, lookAt, vec3(0, 1, 0), 70, aspect, 0.1, (lookAt - lookFrom).length());
    scene.getRenderPass().setMaxDepth(50).setSampleRate(100).setCamera<HitCamera>(
        lookFrom, lookAt, vec3(0, 1, 0), 70.0, aspect);

    // populateScene(scene);
    triangleScene(scene);
    vikingScene(scene);

    scene.render(20, true, true);
    scene.waitIdle();
}