#include "renderpass.hpp"
#include <cstdint>
#include <functional>
#include <mutex>
#include <pew/camera/camera.hpp>
#include <pew/hittable/collection.hpp>
#include <pew/material/material.hpp>
#include <pew/random/random.hpp>
#include <pew/tools/edgeDetection.hpp>
#include <pew/tools/save.hpp>
#include <span>
#include <thread>

namespace pew {

    std::shared_ptr<RenderJob>
    RenderPass::renderColorStage(uint32_t threadCount, bool detectEdges, bool useExistingBuffers) {
        if (!camera) { throw std::runtime_error("Camera not set!"); }
        if (detectEdges && !useExistingBuffers) renderHitStage(threadCount)->join();

        auto job = std::make_shared<RenderJob>();
        for (int i = 0; i < threadCount; ++i) {
            job->threads.emplace_back(
                new std::thread(&RenderPass::renderColor, this, job, detectEdges));
        }

        return job;
    }

    std::shared_ptr<RenderJob>
    RenderPass::renderHitStage(uint32_t threadCount) {
        if (!camera) { throw std::runtime_error("Camera not set!"); }
        auto job = std::make_shared<RenderJob>();

        for (int i = 0; i < threadCount; ++i) {
            job->threads.emplace_back(new std::thread(&RenderPass::renderHits, this, job));
        }
        return job;
    }

    void
    RenderPass::savePng(const std::string& filename) {
        pew::savePng(buffers.color, config.resolution, filename);
    }

    std::vector<Color>
    RenderPass::hitBufferAsColor() {
        std::vector<Color> colorBuffer(buffers.hit.size());
        for (int i = 0; i < buffers.hit.size(); ++i) {
            if (buffers.hit[i]) {
                const auto ptr = reinterpret_cast<uintptr_t>(buffers.hit[i]) * 100;
                colorBuffer[i] = Color { static_cast<unsigned char>(170 * uint8_t(ptr >> 16)),
                                         static_cast<unsigned char>(250 * uint8_t(ptr >> 8)),
                                         static_cast<unsigned char>(100 * uint8_t(ptr)) };
            } else {
                colorBuffer[i] = Color { 0, 0, 0 };
            }
        }
        return colorBuffer;
    }
    std::shared_ptr<Material>
    RenderPass::hitFunction(const Ray& ray) const {
        if (const auto hit = collection.hit(ray)) return hit->material;
        return nullptr;
    }
    void
    RenderPass::renderHits(std::shared_ptr<RenderJob> job) {
        for (auto line = job->line->next(); line < config.resolution.height;
             line      = job->line->next()) {
            auto end = config.resolution.width * (line + 1);
            for (auto i = config.resolution.width * line; i < end; ++i) {
                if (job->shouldStop) return;
                const auto u = double(i % config.resolution.width) / (config.resolution.width - 1);
                const auto v = double(int(config.resolution.height - i / config.resolution.width))
                               / (config.resolution.height - 1);
                buffers.hit[i] = hitFunction(camera->ray(u, v)).get();
            }
        }
    }
    vec3
    RenderPass::colorFunction(const Ray& ray, int depth) const {
        if (depth <= 0) return vec3();

        if (const auto hit = collection.hit(ray)) {
            vec3 attenuation {};
            if (const auto scattered = hit->scatter(attenuation)) {
                return attenuation * colorFunction(scattered.value(), depth - 1);
            }
            return attenuation;
        }

        auto t = 0.5 * (ray.getDirection().y() + 1.0);
        return vec3(1.0 - t) + t * vec3(0.5, 0.7, 1.0);
    }
    void
    RenderPass::renderColor(std::shared_ptr<RenderJob> job, bool detectEdges) {
        std::mutex                   pauseMutex;
        std::unique_lock<std::mutex> pauseLock(pauseMutex);

        for (auto line = job->line->next(); line < config.resolution.height;
             line      = job->line->next()) {
            auto end = config.resolution.width * (line + 1);
            for (auto i = config.resolution.width * line; i < end; ++i) {
                if (config.samples
                    && (!detectEdges || needsSampling(buffers.hit, config.resolution, i, false))) {
                    vec3 pixelColor {};
                    for (int s = 0; s < config.samples; ++s) {
                        if (job->shouldStop) return;
                        job->cvPause->wait(pauseLock, [&job] { return !job->shouldPause; });
                        const auto u = double(i % config.resolution.width + PCG_T::randomDouble())
                                       / (config.resolution.width - 1);
                        const auto v
                            = double(int(config.resolution.height - i / config.resolution.width)
                                     + PCG_T::randomDouble())
                              / (config.resolution.height - 1);
                        pixelColor += colorFunction(camera->ray(u, v), config.maxDepth);
                    }
                    buffers.color[i] = (pixelColor / config.samples).asColor();
                } else {
                    if (job->shouldStop) return;
                    job->cvPause->wait(pauseLock, [&job] { return !job->shouldPause; });
                    auto u = double(i % config.resolution.width) / (config.resolution.width - 1);
                    auto v = double(int(config.resolution.height - i / config.resolution.width))
                             / (config.resolution.height - 1);
                    buffers.color[i] = colorFunction(camera->ray(u, v), config.maxDepth).asColor();
                }
            }
        }
    }
} // namespace pew