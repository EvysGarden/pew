#ifndef PEW_WORLD_RENDERPASS
#define PEW_WORLD_RENDERPASS

#include <condition_variable>
#include <memory>
#include <pew/camera/camera.hpp>
#include <pew/hittable/collection.hpp>
#include <pew/hittable/hittable.hpp>
#include <pew/material/material.hpp>
#include <pew/vector/common.hpp>
#include <stdexcept>
#include <thread>
#include <type_traits>
#include <vector>

namespace pew {
    enum RenderStageEnum
    {
        eRenderStageHits,
        eRenderStageColor
    };

    struct RenderPassConfig final {
        Extent2D resolution;
        uint32_t samples;
        uint32_t maxDepth;
    };

    struct RenderPassBuffers final {
        std::vector<const Material*> hit;
        std::vector<Color>           color;
    };

    struct RenderJob final {
        struct MutexLine final : public MutexVar<uint_fast32_t> {
            uint_fast32_t
            next() {
                mtx.lock();
                auto result = var++;
                mtx.unlock();
                return result;
            }
        };
        std::vector<std::thread*>                threads;
        std::shared_ptr<MutexLine>               line;
        bool                                     shouldPause = false;
        std::shared_ptr<std::condition_variable> cvPause;
        bool                                     shouldStop = false;

        RenderJob() :
            line(std::make_shared<MutexLine>()),
            cvPause(std::make_shared<std::condition_variable>()) {}
        void
        join() {
            for (auto& thread : threads) {
                if (thread->joinable()) { thread->join(); }
                delete thread;
            }
        }

        bool
        paused() {
            return shouldPause;
        }

        void
        pause() {
            shouldPause = true;
        }

        void
        resume() {
            shouldPause = false;
            cvPause->notify_all();
        }

        void
        stop() {
            resume();
            shouldStop = true;
        }
    };

    class RenderPass final {
        const Collection&       collection;
        std::unique_ptr<Camera> camera;
        RenderPassConfig        config;
        RenderPassBuffers       buffers;

        public:
        RenderPass(const Collection& collection) : collection(collection) {}

        RenderPass(const Collection& collection, const Extent2D& viewport) :
            collection(collection), config { viewport } {
            buffers.hit.resize(viewport.width * viewport.height);
            buffers.color.resize(viewport.width * viewport.height);
        }

        RenderPass&
        setResolution(const Extent2D& resolution) {
            config.resolution = resolution;
            buffers.hit.resize(resolution.width * resolution.height);
            buffers.color.resize(resolution.width * resolution.height);
            return *this;
        }

        RenderPass&
        setSampleRate(uint32_t samples) {
            config.samples = samples;
            return *this;
        }

        RenderPass&
        setMaxDepth(uint32_t maxDepth) {
            config.maxDepth = maxDepth;
            return *this;
        }

        RenderPassBuffers&
        getBuffers() {
            return buffers;
        }

        template<typename T, typename... Args>
        requires std::is_base_of_v<Camera, T> RenderPass&
        setCamera(Args&&... args) {
            camera = std::make_unique<T>(std::forward<Args>(args)...);
            return *this;
        }

        template<typename T = Camera>
        requires std::is_base_of_v<Camera, T> T&
        getCamera() {
            return *camera;
        }

        std::vector<Color>
        hitBufferAsColor();

        void
        savePng(const std::string& filename);
        std::shared_ptr<RenderJob>
        renderHitStage(uint32_t threadCount);
        std::shared_ptr<RenderJob>
        renderColorStage(uint32_t threadCount,
                         bool     detectEdges        = true,
                         bool     useExistingBuffers = false);

        private:
        std::shared_ptr<Material>
        hitFunction(const Ray& ray) const;

        vec3
        colorFunction(const Ray& ray, int depth) const;

        void
        renderHits(std::shared_ptr<RenderJob> job);

        void
        renderColor(std::shared_ptr<RenderJob> job, bool detectEdges);
    };

} // namespace pew

#endif