#include "lambertian.hpp"
#include <pew/ray/ray.hpp>

namespace pew::material {
    std::optional<Ray>
    Lambertian::scatter(const Hit& hit, vec3& attenuation) const {
        auto scatterDirection = hit.normal + vec3::randomInUnitSphere();
        attenuation           = albedo;
        if (scatterDirection.nearZero()) return Ray(hit.getPoint(), hit.normal);
        return Ray(hit.getPoint(), scatterDirection);
    }

} // namespace pew::material