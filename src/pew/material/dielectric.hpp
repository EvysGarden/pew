#ifndef PEW_MATERIAL_DIALETRIC
#define PEW_MATERIAL_DIALETRIC

#include <optional>
#include <pew/material/material.hpp>
#include <pew/random/random.hpp>
#include <pew/ray/ray.hpp>
#include <pew/tools/edgeDetection.hpp>
#include <pew/vector/common.hpp>

namespace pew::material {
    struct Dialetric final : public Material {
        const double ir;

        explicit Dialetric(double ir) : ir(ir) {}

        std::optional<Ray>
        scatter(const Hit& hit, vec3& attenuation) const override {
            attenuation                  = vec3(1.0);
            const double refractionRatio = hit.frontFace ? (1.0 / ir) : ir;
            const auto   unitDirection   = hit.ray.getDirection().unit();

            const double cosTheta = fmin((-unitDirection).dot(hit.normal), 1.0);
            const double sinTheta = sqrt(1.0 - cosTheta * cosTheta);

            bool shouldReflect = refractionRatio * sinTheta > 1.0;

            if (shouldReflect || reflectance(cosTheta, refractionRatio) > PCG_T::randomDouble()) {
                return Ray(hit.getPoint(), unitDirection.reflect(hit.normal));
            } else {
                return Ray(hit.getPoint(),
                           unitDirection.refract(hit.normal, refractionRatio, &cosTheta));
            }
        }

        EdgeDetectType
        edgeDetectType() const override {
            return eEdgeDetectNone;
        }

        static double
        reflectance(double cosine, double refractionIndex) {
            auto r0 = (1 - refractionIndex) / (1 + refractionIndex);
            r0      = r0 * r0;
            return r0 + (1 - r0) * pow((1 - cosine), 5);
        }
    };
} // namespace pew::material

#endif