#ifndef PEW_MATERIAL_OPAQUE
#define PEW_MATERIAL_OPAQUE

#include <pew/vector/common.hpp>
#include <pew/material/material.hpp>
#include <pew/hittable/hittable.hpp>

namespace pew::material {
    struct Opaque final : public Material {
        vec3 albedo;

        inline Opaque() = default;
        inline Opaque(vec3 albedo) : albedo(albedo) {}

        constexpr EdgeDetectType edgeDetectType() const override {
            return eEdgeDetectHit;
        }

        std::optional<Ray> scatter(const Hit &hit, vec3 &attenuation) const override {
            attenuation = albedo;
            return std::nullopt;
        }
    };
}

#endif