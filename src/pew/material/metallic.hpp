#ifndef PEW_MATERIAL_METALLIC
#define PEW_MATERIAL_METALLIC

#include <pew/material/material.hpp>
#include <pew/ray/ray.hpp>
#include <pew/vector/common.hpp>

namespace pew::material {
    struct Metallic final : public Material {
        const vec3   albedo;
        const double fuzz;

        public:
        explicit Metallic(const vec3& albedo, double f) : albedo(albedo), fuzz(f < 1 ? f : 1) {}

        std::optional<Ray>
        scatter(const Hit& hit, vec3& attenuation) const override {
            auto reflection = hit.ray.getDirection().unit().reflect(hit.normal);
            attenuation     = albedo;
            if (reflection.dot(hit.normal) > 0) {
                if (fuzz == 0.0) {
                    return Ray { hit.getPoint(), reflection };
                } else {
                    return Ray { hit.getPoint(), reflection + fuzz * vec3::randomInUnitSphere() };
                }
            }
            return std::nullopt;
        }

        EdgeDetectType
        edgeDetectType() const override {
            return eEdgeDetectNone;
        }
    };
} // namespace pew::material

#endif