#ifndef PEW_MATERIAL_NORMAL
#define PEW_MATERIAL_NORMAL

#include <pew/hittable/hittable.hpp>
#include <pew/vector/common.hpp>
#include <optional>
#include <pew/material/material.hpp>

namespace pew::material {
    struct Normal final : public Material {
        constexpr EdgeDetectType edgeDetectType() const override {
            return eEdgeDetectHit;
        }

        std::optional<Ray> scatter(const Hit &hit, vec3 &attenuation) const override {
            attenuation = 0.5 * (hit.normal + vec3(1));
            return std::nullopt;
        }
    };
} // namespace pew

#endif