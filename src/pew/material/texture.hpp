#ifndef PEW_MATERIAL_TEXTURE
#define PEW_MATERIAL_TEXTURE

#include <pew/material/material.hpp>
#include <pew/texture/holder.hpp>

#include <filesystem>
#include <vector>

namespace pew::material {
    struct Texture final : public Material {
        std::shared_ptr<TextureHolder> texture;

        inline Texture(std::shared_ptr<TextureHolder> texture) : texture(texture) {}

        inline EdgeDetectType
        edgeDetectType() const override {
            return eEdgeDetectHit;
        }

        std::optional<Ray>
        scatter(const Hit& hit, vec3& attenuation) const override {
            attenuation = texture->at(hit.tex[0], hit.tex[1]);
            return std::nullopt;
        }
    };
} // namespace pew::material

#endif