#ifndef PEW_MATERIAL_MATERIAL
#define PEW_MATERIAL_MATERIAL

#include <optional>
#include <pew/hittable/hittable.hpp>
#include <pew/tools/edgeDetection.hpp>
#include <pew/vector/common.hpp>

namespace pew {
    struct Material {
        virtual std::optional<Ray>
        scatter(const Hit& hit, vec3& attenuation) const = 0;

        virtual ~Material() {}

        virtual EdgeDetectType
        edgeDetectType() const = 0;
    };
} // namespace pew

#endif