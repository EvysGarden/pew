#ifndef PEW_MATERIAL_DIFFUSE
#define PEW_MATERIAL_DIFFUSE

#include <pew/material/material.hpp>
#include <pew/ray/ray.hpp>
#include <pew/tools/edgeDetection.hpp>
#include <pew/vector/common.hpp>

namespace pew::material {
    struct Lambertian final : public Material {
        vec3 albedo;
        explicit Lambertian(const vec3& albedo) : albedo(albedo) {}

        std::optional<Ray>
        scatter(const Hit& hit, vec3& attenuation) const override;

        constexpr EdgeDetectType
        edgeDetectType() const override {
            return eEdgeDetectNone;
        }
    };
} // namespace pew::material

#endif