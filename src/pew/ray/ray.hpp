#ifndef PEW_RAY_RAY
#define PEW_RAY_RAY

#include <limits>
#include <pew/vector/common.hpp>

namespace pew {
    class Ray final {
        vec3  origin;
        vec3  direction;
        Range range;

        public:
        constexpr Ray() = default;
        constexpr Ray(const vec3& origin, const vec3& direction, const Range& range) :
            origin(origin), direction(direction), range(range) {}

        constexpr Ray(const vec3& origin, const vec3& direction) :
            origin(origin), direction(direction),
            range(0.001, std::numeric_limits<double>::infinity()) {}

        constexpr auto
        getOrigin() const {
            return origin;
        }
        constexpr auto
        getDirection() const {
            return direction;
        }
        constexpr auto
        getRange() const {
            return range;
        }

        constexpr void
        setRange(const Range& range) {
            this->range = range;
        }
        
        constexpr const auto
        operator()(double t) const {
            return origin + t * direction;
        }
    };
} // namespace pew

#endif