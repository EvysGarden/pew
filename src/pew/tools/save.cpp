#include "save.hpp"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb/stb_image_write.h>

namespace pew {
    void
    savePng(const std::vector<Color>& buffer,
            const Extent2D&           resolution,
            const std::string&        filename) {
        stbi_write_png(filename.c_str(),
                       resolution.width,
                       resolution.height,
                       3,
                       buffer.data(),
                       resolution.width * 3);
    }
} // namespace pew