#ifndef PEW_TOOLS_EDGE_DETECTION
#define PEW_TOOLS_EDGE_DETECTION

#include <array>
#include <pew/vector/common.hpp>
#include <span>

namespace pew {
    enum EdgeDetectType
    {
        eEdgeDetectNone,
        eEdgeDetectHit,
        eEdgeDetectColor,
        eEdgeDetectCustom
    };

    enum Direction
    {
        eDirectionLeft,
        eDirectionRight,
        eDirectionUp,
        eDirectionDown,
    };

    template<typename T>
    constexpr bool
    isEdgeLine(std::span<T> buffer, Extent2D size, size_t index, Direction direction, int length) {
        switch (direction) {
            case eDirectionDown: {
                auto prevIndex = index;
                for (int i = 0; i < length; ++i) {
                    auto curIndex = index + i * size.width;
                    if (curIndex < buffer.size()) {
                        if (buffer[prevIndex] != buffer[curIndex]) return true;
                    } else {
                        return false;
                    }
                    prevIndex = curIndex;
                }
                return false;
            }

            case eDirectionUp: {
                auto prevIndex = index;
                for (int i = 1; i <= length; ++i) {
                    auto curIndex = prevIndex - size.width;
                    if (curIndex >= 0 && curIndex < prevIndex) {
                        if (buffer[prevIndex] != buffer[curIndex]) return true;
                    } else {
                        return false;
                    }
                    prevIndex = curIndex;
                }
                return false;
            }

            case eDirectionRight: {
                for (int i = index + 1; (i < (index + length)) && !(i % size.width); ++i) {
                    if (buffer[i] != buffer[i - 1]) return true;
                }
                return false;
            }

            case eDirectionLeft: {
                for (int i = index - 1; (i > (index - length)) && !(i + 1 % size.width); ++i) {
                    if (buffer[i] != buffer[i + 1]) return true;
                }
                return false;
            }

            default: return true;
        }
    }

    template<typename T>
    constexpr bool
    isEdge(std::span<T> buffer, Extent2D size, size_t index) {
#if defined(PEW_EDGE_DETECTION_ALL)
        return true;
#elif defined(PEW_EDGE_DETECTION_NONE)
        return false;
#endif

#if !defined(PEW_EDGE_DETECTION_STRONG)
        // Upper left corner
        if (index == 0) {
            return buffer[0] != buffer[1] || buffer[0] != buffer[size.width]
                   || buffer[0] != buffer[size.width + 1];
        }

        // Upper right corner
        if (index == size.width - 1) {
            return buffer[size.width - 1] != buffer[size.width - 2]
                   || buffer[size.width - 1] != buffer[2 * size.width - 1]
                   || buffer[size.width - 1] != buffer[2 * size.width - 2];
        }

        // Lower left corner
        if (index == (size.height - 1) * size.width) {
            return buffer[index] != buffer[index + 1] || buffer[index] != buffer[index - size.width]
                   || buffer[index] != buffer[index - size.width + 1];
        }

        // Lower right corner
        if (index == size.width * size.height - 1) {
            return buffer[index] != buffer[index - 1] || buffer[index] != buffer[index - size.width]
                   || buffer[index] != buffer[index - size.width - 1];
        }

        // left edge
        if (!(index % size.width)) {
            return buffer[index] != buffer[index + 1] || buffer[index] != buffer[index - size.width]
                   || buffer[index] != buffer[index + size.width]
                   || buffer[index] != buffer[index - size.width + 1]
                   || buffer[index] != buffer[index + size.width + 1];
        }

        // right edge
        if (!((index + 1) % size.width)) {
            return buffer[index] != buffer[index - 1] || buffer[index] != buffer[index - size.width]
                   || buffer[index] != buffer[index + size.width]
                   || buffer[index] != buffer[index - size.width - 1]
                   || buffer[index] != buffer[index + size.width - 1];
        }

        // top edge
        if (index < size.width) {
            return buffer[index] != buffer[index - 1] || buffer[index] != buffer[index + 1]
                   || buffer[index] != buffer[index + size.width - 1]
                   || buffer[index] != buffer[index + size.width]
                   || buffer[index] != buffer[index + size.width + 1];
        }

        // bottom edge
        if (index >= buffer.size() - size.width) {
            return buffer[index] != buffer[index - 1] || buffer[index] != buffer[index + 1]
                   || buffer[index] != buffer[index - size.width - 1]
                   || buffer[index] != buffer[index - size.width]
                   || buffer[index] != buffer[index - size.width + 1];
        }

        // rest
        return buffer[index] != buffer[index - 1] || buffer[index] != buffer[index + 1]
               || buffer[index] != buffer[index - size.width]
               || buffer[index] != buffer[index + size.width]
               || buffer[index] != buffer[index - size.width - 1]
               || buffer[index] != buffer[index - size.width + 1]
               || buffer[index] != buffer[index + size.width - 1]
               || buffer[index] != buffer[index + size.width + 1];
#else
        return isEdgeLine(buffer, size, index, eDirectionDown, 20)
               || isEdgeLine(buffer, size, index, eDirectionLeft, 20)
               || isEdgeLine(buffer, size, index, eDirectionUp, 20)
               || isEdgeLine(buffer, size, index, eDirectionRight, 20);
#endif
    }

    class Material;
    bool
    needsSampling(const std::vector<const Material*>& hitBuffer,
                  const Extent2D&                     view,
                  size_t                              index,
                  bool                                colorStage);
} // namespace pew

#endif