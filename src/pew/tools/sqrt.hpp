#ifndef PEW_TOOLS_SQRT
#define PEW_TOOLS_SQRT

#include <limits>
#include <type_traits>

namespace pew {
    // NOTE: 6 AM code from
    // https://stackoverflow.com/questions/8622256/in-c11-is-sqrt-defined-as-constexpr
    namespace Detail {
        template<typename T>
        requires std::is_floating_point_v<T> T constexpr sqrtNewtonRaphson(T x, T curr, T prev) {
            return curr == prev ? curr : sqrtNewtonRaphson(x, (T)0.5 * (curr + x / curr), curr);
        }
    } // namespace Detail

    /*
     * Constexpr version of the square root
     * Return value:
     *   - For a finite and non-negative value of "x", returns an approximation for the square root
     * of "x"
     *   - Otherwise, returns NaN
     */
    template<typename T>
    requires std::is_floating_point_v<T> T constexpr sqrt(T x) {
        return x >= 0 && x < std::numeric_limits<T>::infinity()
                   ? Detail::sqrtNewtonRaphson(x, x, (T)0)
                   : std::numeric_limits<T>::quiet_NaN();
    }
}

#endif