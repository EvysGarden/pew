#include "edgeDetection.hpp"
#include <pew/material/material.hpp>

namespace pew {
    bool
    needsSampling(const std::vector<const Material*>& hitBuffer,
                  const Extent2D&                     view,
                  size_t                              index,
                  bool                                colorStage) {
        if (colorStage) {
            return !isEdge(std::span(hitBuffer), view, index) && hitBuffer[index]
                   && hitBuffer[index]->edgeDetectType() == eEdgeDetectColor;
        } else {
            return isEdge(std::span(hitBuffer), view, index)
                   || (hitBuffer[index] && hitBuffer[index]->edgeDetectType() == eEdgeDetectNone);
        }
    }
} // namespace pew