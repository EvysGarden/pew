#ifndef PEW_TOOLS_SAVE
#define PEW_TOOLS_SAVE

#include <pew/vector/common.hpp>
#include <vector>
namespace pew {
    void
    savePng(const std::vector<Color>& buffer,
            const Extent2D&           resolution,
            const std::string&        filename);
}

#endif