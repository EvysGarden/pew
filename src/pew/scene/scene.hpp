#ifndef PEW_SCENE_SCENE
#define PEW_SCENE_SCENE

#include <pew/camera/camera.hpp>
#include <pew/hittable/collection.hpp>
#include <pew/hittable/hittable.hpp>
#include <pew/renderpass/renderpass.hpp>
#include <pew/texture/holder.hpp>
#include <pew/vector/common.hpp>
#include <pew/window/window.hpp>

#include <SDL2/SDL_keycode.h>
#include <deque>
#include <memory>
#include <sstream>
#include <type_traits>
#include <utility>

namespace pew {
    class Scene final {
        Extent2D                                    extent;
        Collection                                  collection;
        std::vector<std::shared_ptr<Material>>      materials;
        std::vector<std::shared_ptr<TextureHolder>> textures;
        RenderPass                                  renderPass;
        Window                                      window;
        std::shared_ptr<RenderJob>                  job;
        std::vector<Color>                          hitBufferAsColor;

        public:
        Scene(const Extent2D& extent);

        void
        render(uint32_t threadCount, bool detectEdges, bool showWindow);

        void
        waitIdle();

        private:
        void
        onShouldClose();

        void
        onKeyInput(SDL_KeyboardEvent event, bool down);

        public:
        template<typename T, typename... Args>
        auto&
        add(Args&&... args) {
            return collection.add<T>(std::forward<Args>(args)...);
        }

        template<typename T, typename... Args>
        auto&
        addMaterial(Args&&... args) {
            return materials.emplace_back(new T(std::forward<Args>(args)...));
        }

        template<typename... Args>
        auto&
        addTexture(Args&&... args) {
            return textures.emplace_back(new TextureHolder(std::forward<Args>(args)...));
        }

        template<typename T, typename... Args>
        requires std::is_base_of_v<Camera, T>
        void
        setCamera(Args&&... args) {
            renderPass.setCamera<T>(std::forward<Args>(args)...);
        }

        inline auto&
        getRenderPass() {
            return renderPass;
        }

        inline auto&
        getWindow() {
            return window;
        }
    };
} // namespace pew

#endif