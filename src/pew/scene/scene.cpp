#include "scene.hpp"
#include <pew/tools/save.hpp>

namespace pew {

    Scene::Scene(const Extent2D& extent) :
        extent(extent), renderPass(collection, extent), window(extent) {
        window.setShouldCloseCallback([&] { this->onShouldClose(); });
        window.setKeyInputCallback(
            [&](SDL_KeyboardEvent event, bool down) { this->onKeyInput(event, down); });
        window.addBuffer(&renderPass.getBuffers().color);
        std::stringstream str;
        str << "Rendering at " << extent.width << "x" << extent.height;
        window.setTitle(str.str());
    }
    void
    Scene::render(uint32_t threadCount, bool detectEdges, bool showWindow) {
        if (showWindow) window.startAndFork();

        if (detectEdges) {
            renderPass.renderHitStage(threadCount)->join();
            hitBufferAsColor = renderPass.hitBufferAsColor();
            window.addBuffer(&hitBufferAsColor);
        }

        job = renderPass.renderColorStage(threadCount, detectEdges, true);
    }
    void
    Scene::waitIdle() {
        if (job) job->join();
        window.join();
    }
    void
    Scene::onShouldClose() {
        window.closeWindow();
        if (job) job->stop();
    }
    void
    Scene::onKeyInput(SDL_KeyboardEvent event, bool down) {
        if (down) {
            switch (event.keysym.sym) {
                case SDLK_u: window.setIndex(0); break;
                case SDLK_i:
                    if (window.getBuffers().size() > 1) window.setIndex(1);
                    break;
                case SDLK_SPACE: {
                    if (job) { job->paused() ? job->resume() : job->pause(); }
                    break;
                }
                case SDLK_s: {
                    savePng(*window.getCurrentBuffer(), extent, "shot.png");
                    break;
                }
                default: break;
            }
        }
    }
} // namespace pew