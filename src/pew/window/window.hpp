#ifndef PEW_WORLD_PREVIEW
#define PEW_WORLD_PREVIEW

#include <SDL2/SDL.h>
#include <SDL2/SDL_error.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_hints.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_video.h>
#include <atomic>
#include <functional>
#include <iostream>
#include <pew/vector/common.hpp>
#include <shared_mutex>
#include <sstream>
#include <stdexcept>
#include <thread>
#include <vector>

namespace pew {
    class Window final {
        using KeyInputCallback    = std::function<void(SDL_KeyboardEvent event, bool keyDown)>;
        using ShouldCloseCallback = std::function<void()>;
        using ColorBuffer         = std::vector<Color>;

        SDL_Renderer*             renderer;
        SDL_Window*               window;
        SDL_Texture*              texture;
        std::vector<ColorBuffer*> buffers;
        std::mutex                buffersMutex;
        size_t                    index = 0;
        Extent2D                  view;

        std::thread* thread = nullptr;

        KeyInputCallback    keyInputCallback;
        ShouldCloseCallback shouldCloseCallback;
        bool                shouldClose = false;

        std::string title;

        public:
        Window(const Extent2D& view) : view(view) {
            if (SDL_Init(SDL_INIT_VIDEO) < 0) { throw std::runtime_error(SDL_GetError()); }

            auto rendererFlags = SDL_RENDERER_ACCELERATED;
            auto windowFlags   = 0;
            if (!(window = SDL_CreateWindow("pew",
                                            SDL_WINDOWPOS_UNDEFINED,
                                            SDL_WINDOWPOS_UNDEFINED,
                                            view.width,
                                            view.height,
                                            windowFlags))) {
                throw std::runtime_error(SDL_GetError());
            }

            SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest");
            if (!(renderer = SDL_CreateRenderer(window, -1, rendererFlags))) {
                throw std::runtime_error(SDL_GetError());
            }
            if (!(texture = SDL_CreateTexture(renderer,
                                              SDL_PIXELFORMAT_RGB24,
                                              SDL_TEXTUREACCESS_STATIC,
                                              view.width,
                                              view.height))) {
                throw std::runtime_error(SDL_GetError());
            }
            TTF_Init();
        }

        void
        setTitle(const std::string& title) {
            this->title = title;
        }

        operator auto() { return window; }

        void
        setIndex(size_t index) {
            this->index = index;
        }

        auto
        getIndex() const noexcept {
            return index;
        }

        auto& getBuffers() {
            return buffers;
        }

        auto
        getCurrentBuffer() {
            return buffers[index];
        }

        void
        setKeyInputCallback(KeyInputCallback callback) {
            keyInputCallback = callback;
        }

        void
        setShouldCloseCallback(ShouldCloseCallback callback) {
            shouldCloseCallback = callback;
        }

        size_t
        addBuffer(ColorBuffer* buffer) {
            buffersMutex.lock();
            buffers.push_back(buffer);
            auto index = buffers.size() - 1;
            buffersMutex.unlock();
            return index;
        }

        void
        resize(const Extent2D& view) {
            this->view = view;
            SDL_DestroyTexture(texture);
            if (!(texture = SDL_CreateTexture(renderer,
                                              SDL_PIXELFORMAT_RGB24,
                                              SDL_TEXTUREACCESS_STATIC,
                                              view.width,
                                              view.height))) {
                throw std::runtime_error(SDL_GetError());
            }
            std::stringstream titleBuilder;
            titleBuilder << "Pew: " << view.width << "x" << view.height;
            title = titleBuilder.str();
        }

        ~Window() {
            SDL_DestroyTexture(texture);
            SDL_DestroyRenderer(renderer);
            SDL_DestroyWindow(window);
            SDL_Quit();
        }

        void
        closeWindow() {
            shouldClose = true;
        }

        void
        handleInput() {
            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                switch (event.type) {
                    case SDL_QUIT:
                        if (shouldCloseCallback) shouldCloseCallback();
                        break;
                    case SDL_KEYDOWN:
                        if (keyInputCallback) keyInputCallback(event.key, true);
                        break;
                    case SDL_KEYUP:
                        if (keyInputCallback) keyInputCallback(event.key, false);
                        break;
                }
            }
        }

        void
        renderTitle() {
            if (title.empty()) return;
            TTF_Font* font
                = TTF_OpenFont("/usr/share/fonts/liberation/LiberationSans-Regular.ttf", 21);

            SDL_Color colorBack  = { 0, 0, 0 };
            SDL_Color colorFront = { 255, 255, 255 };

            auto surfaceBack  = TTF_RenderText_Solid(font, title.c_str(), colorBack);
            auto surfaceFront = TTF_RenderText_Solid(font, title.c_str(), colorFront);

            SDL_GetError();
            auto textureBack  = SDL_CreateTextureFromSurface(renderer, surfaceBack);
            auto textureFront = SDL_CreateTextureFromSurface(renderer, surfaceFront);

            SDL_Rect rect;
            rect.x = 2;
            rect.y = 2;
            rect.w = surfaceBack->w;
            rect.h = surfaceBack->h;
            SDL_RenderCopy(renderer, textureBack, NULL, &rect);

            rect.x = 0;
            rect.y = 0;
            SDL_RenderCopy(renderer, textureFront, NULL, &rect);
        }

        std::thread*
        startAndFork() {
            thread = new std::thread(&Window::start, this);
            return thread;
        }

        void
        join() {
            if (thread && thread->joinable()) thread->join();
        }

        void
        start() {
            while (!shouldClose) {
                handleInput();
                if (!buffers.empty()) {
                    if (auto result = SDL_UpdateTexture(
                            texture, NULL, buffers[index]->data(), view.width * 3)) {
                        throw std::runtime_error(SDL_GetError());
                    }
                }

                SDL_RenderClear(renderer);
                if (!buffers.empty()) {
                    if (auto result = SDL_RenderCopy(renderer, texture, NULL, NULL)) {
                        throw std::runtime_error(SDL_GetError());
                    }
                }

                renderTitle();

                SDL_RenderPresent(renderer);
                SDL_Delay(16);
            }
        }
    };
} // namespace pew

#endif