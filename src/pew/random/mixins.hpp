#ifndef PEW_RANDOM_MIXINS
#define PEW_RANDOM_MIXINS

#include <cstdint>
#include <cstdlib>
#include <limits>
namespace pew {
    template<typename BASE>
    class RandomMixin {
        public:
        inline static uint32_t
        random(int min, int max) {
            return min + BASE::random() % (max - min);
        }

        inline static double
        randomDouble() {
            return BASE::random() / (BASE::max() + 1.0);
        }

        inline static double
        randomDouble(double lo, double hi) {
            return lo + (hi - lo) * randomDouble();
        }
    };

    template<typename BASE>
    class ConstRandomMixin {
        public:
        virtual uint32_t
        random()
            = 0;
        uint32_t
        random(int min, int max) {
            return min + random() % (max - min);
        }

        double
        randomDouble() {
            return double(random()) / (max() + 1.0);
        }

        double
        randomDouble(double lo, double hi) {
            return lo + (hi - lo) * randomDouble();
        }

        static uint32_t
        min() {
            return std::numeric_limits<uint32_t>::min();
        }

        static uint32_t
        max() {
            return std::numeric_limits<uint32_t>::max();
        }
    };
} // namespace pew

#endif