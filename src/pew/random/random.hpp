#ifndef PEW_RANDOM
#define PEW_RANDOM

#include <climits>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <limits>
#include <pew/random/mixins.hpp>
#include <random>

namespace pew {
    struct PCG_T final : public RandomMixin<PCG_T> {
        inline static uint64_t state = 0;
        inline static uint64_t inc   = 0;

        inline static uint32_t
        random() {
            auto oldState   = state;
            state           = oldState * 6364136223846793005ULL + (inc | 1);
            auto xorShifted = ((oldState >> 18u) ^ oldState) >> 27u;
            auto rot        = oldState >> 59u;
            return (xorShifted >> rot) | (xorShifted << ((-rot) & 31));
        }

        inline static auto
        max() {
            return std::numeric_limits<uint32_t>::max();
        }
    };

    struct PCG_CONST_T final : public ConstRandomMixin<PCG_CONST_T> {
        uint64_t state = 0;
        uint64_t inc   = 0;

        PCG_CONST_T() = default;
        explicit PCG_CONST_T(uint64_t state) : state(state) {}

        uint32_t
        random() override {
            auto oldState   = state;
            state           = oldState * 6364136223846793005ULL + (inc | 1);
            auto xorShifted = ((oldState >> 18u) ^ oldState) >> 27u;
            auto rot        = oldState >> 59u;
            return (xorShifted >> rot) | (xorShifted << ((-rot) & 31));
        }
    };

    struct STDRAND_T : public RandomMixin<STDRAND_T> {
        inline static uint32_t
        random() {
            return rand();
        }

        inline static auto
        max() {
            return RAND_MAX;
        }
    };
} // namespace pew

#endif