#ifndef PEW_TEXTURE_HOLDER
#define PEW_TEXTURE_HOLDER

#include <filesystem>
#include <pew/vector/common.hpp>

namespace pew {
    class TextureHolder final {
        std::vector<vec3> colors;
        int               width = 0, height = 0;

        public:
        TextureHolder(const std::filesystem::path& path);

        vec3
        at(double u, double v) const {
            if (u < 0. || u > 1. || v < 0. || v > 1.) return vec3(1, 0, 0);

            if (colors.empty()) return vec3(1, 0, 0);

            // turn v around because reasons...
            v = 1 - v;

            size_t x = (u * width) + .499;
            size_t y = (v * height) + .499;
            return colors[width * y + x];
        }
    };
} // namespace pew
#endif