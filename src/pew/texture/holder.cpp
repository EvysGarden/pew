#include "holder.hpp"

#include <pew/vector/common.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

namespace pew {
    TextureHolder::TextureHolder(const std::filesystem::path& path) {
        auto data = stbi_load(path.c_str(), &width, &height, nullptr, 3);
        if (!data) return;
        size_t size = width * height;

        colors.reserve(size);

        for (size_t i = 0; i < size; ++i) {
            colors.emplace_back(static_cast<uint8_t>(data[3 * i + 0]) / 256.,
                                static_cast<uint8_t>(data[3 * i + 1]) / 256.,
                                static_cast<uint8_t>(data[3 * i + 2]) / 256.);
        }
    }
} // namespace pew