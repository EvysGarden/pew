#ifndef PEW_WORLD_CAMERA
#define PEW_WORLD_CAMERA

#include <math.h>
#include <optional>
#include <pew/ray/ray.hpp>
#include <pew/vector/common.hpp>
#include <type_traits>
#include <utility>
namespace pew {
    struct Camera {
        virtual void
        move(vec3 relative)
            = 0;
        virtual void
        moveTo(vec3 absolute)
            = 0;
        virtual Ray
        ray(double s, double t) const = 0;

        template<typename T>
        requires std::is_base_of_v<Camera, std::remove_reference_t<T>> T&
        as() {
            return dynamic_cast<T&>(*this);
        }

        virtual void
        set(Camera& other)
            = 0;
        
        virtual ~Camera() {};
    };

} // namespace pew

#endif