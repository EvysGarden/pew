#ifndef PEW_CAMERA_DEPTH_CAMERA
#define PEW_CAMERA_DEPTH_CAMERA

#include "camera.hpp"

namespace pew {
    struct DepthCamera final : public Camera {
        vec3   origin;
        vec3   horizontal;
        vec3   vertical;
        vec3   corner;
        vec3   u, v, w;
        double lensRadius;

        DepthCamera() = default;

        DepthCamera(vec3                  lookFrom,
                    vec3                  lookAt,
                    vec3                  vUp,
                    double                vfov,
                    double                aspect,
                    double                aperture,
                    std::optional<double> focusDistance) {
            auto                      theta    = degToRad(vfov);
            auto                      h        = tan(theta / 2);
            std::pair<double, double> viewport = { aspect * 2.0 * h, 2.0 * h };

            w = (lookFrom - lookAt).unit();
            u = vUp.cross(w).unit();
            v = w.cross(u);

            origin = lookFrom;

            double f_focusDistance;
            if (focusDistance.has_value()) {
                f_focusDistance = focusDistance.value();
            } else {
                f_focusDistance = (lookFrom - lookAt).length();
            }
            horizontal = f_focusDistance * viewport.first * u;
            vertical   = f_focusDistance * viewport.second * v;
            corner     = origin - horizontal / 2 - vertical / 2 - f_focusDistance * w;

            lensRadius = aperture / 2;
        }

        void
        set(Camera& other) override {
            *this = other.as<DepthCamera>();
        }

        void
        move(vec3 relative) override {
            origin += relative;
            corner += relative;
        }

        void
        moveTo(vec3 position) override {
            move(position - origin);
        }

        Ray
        ray(double s, double t) const override {
            auto rd     = lensRadius * vec3::randomInUnitDisk();
            auto offset = u * rd.x() + v * rd.y();
            return { origin + offset, corner + s * horizontal + t * vertical - origin - offset };
        }
    };
} // namespace pew

#endif