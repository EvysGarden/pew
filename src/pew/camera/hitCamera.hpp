#ifndef PEW_HIT_CAMERA
#define PEW_HIT_CAMERA

#include "camera.hpp"

namespace pew {
    class HitCamera final : public Camera {
        vec3 origin;
        vec3 horizontal;
        vec3 vertical;
        vec3 corner;

        public:
        constexpr HitCamera() = default;

        constexpr HitCamera(const vec3& origin, const Viewport& viewport, double focal) :
            origin(origin), horizontal(viewport.first, 0, 0), vertical(0, viewport.second, 0),
            corner(origin - horizontal / 2 - vertical / 2 - vec3(0, 0, focal)) {}

        constexpr HitCamera(const vec3& origin,
                            const vec3& horizontal,
                            const vec3& vertical,
                            const vec3& corner) :
            origin(origin),
            horizontal(horizontal), vertical(vertical), corner(corner) {}

        constexpr HitCamera(vec3 lookFrom, vec3 lookAt, vec3 vUp, double vfov, double aspect) {
            auto                      theta    = degToRad(vfov);
            auto                      h        = tan(theta / 2);
            std::pair<double, double> viewport = { aspect * 2.0 * h, 2.0 * h };

            auto w = (lookFrom - lookAt).unit();
            auto u = vUp.cross(w).unit();
            auto v = w.cross(u);

            origin = lookFrom;

            horizontal = viewport.first * u;
            vertical   = viewport.second * v;
            corner     = origin - horizontal / 2 - vertical / 2 - w;
        }

        constexpr void
        move(vec3 relative) override {
            origin += relative;
            corner += relative;
        }

        constexpr void
        moveTo(vec3 position) override {
            move(position - origin);
        }

        constexpr Ray
        ray(double s, double t) const override {
            return { origin, corner + s * horizontal + t * vertical - origin };
        }

        void
        set(Camera& other) override {
            *this = other.as<HitCamera>();
        }
    };
} // namespace pew

#endif