#ifndef PEW_VECTOR_COMMON
#define PEW_VECTOR_COMMON

#include <cmath>
#include <cstdint>
#include <mutex>
#if defined(PEW_USE_NAIVE_VECTORS)
#include <pew/vector/naiveVector.hpp>
#elif defined(PEW_USE_REFERENCE_VECTORS)
#include <array>
#include <pew/vector/reference.hpp>
#else
#include <pew/vector/vector.hpp>
#endif

namespace pew {
#if defined(PEW_USE_NAIVE_VECTORS)
    using vec3  = NaiveVector<double>;
    using vec3i = NaiveVector<int>;
    using Color = NaiveVector<int>::color_type;
#elif defined(PEW_USE_REFERENCE_VECTORS)
    using vec3 = RefVector;
    // using vec3i = RefVector<int>;
    using Color = std::array<uint8_t, 3>;
#else
    using vec3  = Vector<double>;
    using vec3i = Vector<int>;
    using Color = Vector<int>::color_type;
#endif

    using Viewport = std::pair<double, double>;
    using Range    = std::pair<double, double>;

    struct Extent2D final {
        uint32_t width;
        uint32_t height;

        Extent2D() = default;
        Extent2D(uint32_t width, uint32_t height) : width(width), height(height) {}

        Extent2D
        operator*(uint32_t t) const {
            return { width * t, height * t };
        }

        Extent2D
        operator/(uint32_t t) const {
            return { width / t, height / t };
        }
    };

    

    constexpr double
    degToRad(double degrees) {
        return degrees * M_PI / 180.0;
    }

    template<typename T>
    struct MutexVar {
        mutable std::mutex mtx;
        T          var;
    };
} // namespace pew

#endif