#ifndef PEW_VECTOR_NAIVE_VECTOR
#define PEW_VECTOR_NAIVE_VECTOR

#include <pew/random/random.hpp>
#include <pew/tools/sqrt.hpp>

#include <algorithm>
#include <array>
#include <cmath>
#include <ostream>
#include <type_traits>
#include <variant>

namespace pew {
    template<typename T>
    class NaiveVector final {
        using random_type = PCG_T;

        public:
        using color_type = std::array<uint8_t, 3>;

        private:
        std::array<T, 3> v;

        public:
        constexpr NaiveVector(T x, T y, T z) { v = std::array { x, y, z }; }
        explicit constexpr NaiveVector(T c) { v = std::array { c, c, c }; }
        constexpr NaiveVector() { v = std::array<T, 3> { 0, 0, 0 }; }

        constexpr T
        x() const {
            return v[0];
        }
        constexpr T
        y() const {
            return v[1];
        }
        constexpr T
        z() const {
            return v[2];
        }

        constexpr bool
        operator==(const NaiveVector& other) const {
            return v[0] == other.v[0] && v[1] == other.v[1] && v[2] == other.v[2];
        }

        constexpr NaiveVector
        operator-() const {
            return { -v[0], -v[1], -v[2] };
        }

        constexpr NaiveVector
        operator+(const NaiveVector& other) const {
            return { v[0] + other.v[0], v[1] + other.v[1], v[2] + other.v[2] };
        }

        constexpr NaiveVector
        operator-(const NaiveVector& other) const {
            return { v[0] - other.v[0], v[1] - other.v[1], v[2] - other.v[2] };
        }

        constexpr NaiveVector
        operator+=(const NaiveVector& other) {
            v[0] += other.v[0];
            v[1] += other.v[1];
            v[2] += other.v[2];
            return *this;
        }

        constexpr NaiveVector
        operator-=(const NaiveVector& other) {
            v[0] -= other.v[0];
            v[1] -= other.v[1];
            v[2] -= other.v[2];
            return *this;
        }

        constexpr NaiveVector
        operator*(const T& s) const {
            return { v[0] * s, v[1] * s, v[2] * s };
        }

        constexpr NaiveVector&
        operator*=(const T& s) {
            v[0] *= s;
            v[1] *= s;
            v[2] *= s;
            return *this;
        }

        constexpr NaiveVector
        operator*(const NaiveVector& other) const {
            return { v[0] * other.v[0], v[1] * other.v[1], v[2] * other.v[2] };
        }

        constexpr T
        dot(const NaiveVector& other) const {
            return v[0] * other.v[0] + v[1] * other.v[1] + v[2] * other.v[2];
        }

        constexpr NaiveVector
        cross(const NaiveVector& other) const {
            return { v[1] * other.v[2] - v[2] * other.v[1],
                     v[2] * other.v[0] - v[0] * other.v[2],
                     v[0] * other.v[1] - v[1] * other.v[0] };
        }

        constexpr NaiveVector
        operator/(const T& s) const {
            return (1 / s) * *this;
            // return { v[0] / s, v[1] / s, v[2] / s };
        }

        constexpr NaiveVector&
        operator/=(const T& s) {
            v[0] /= s;
            v[1] /= s;
            v[2] /= s;
            return *this;
        }

        constexpr T
        squareLength() const {
            return dot(*this);
        }

        constexpr T
        length() const {
            return pew::sqrt(squareLength());
        }

        constexpr NaiveVector
        unit() const {
            return (*this) / length();
        }

        constexpr NaiveVector
        abs() const {
            return { std::abs(v[0]), std::abs(v[1]), std::abs(v[2]) };
        }

        constexpr T
        max() const {
            return std::max(v[0], std::max(v[1], v[2]));
        }

        constexpr T
        acos(const NaiveVector& other) const {
            return std::acos((*this * other) / pew::sqrt(squareLength() * other.squareLength()));
        }

        constexpr color_type
        asColor() const {
            return { static_cast<uint8_t>(std::clamp(pew::sqrt(v[0]), (T)0.0, (T)0.999) * 256),
                     static_cast<uint8_t>(std::clamp(pew::sqrt(v[1]), (T)0.0, (T)0.999) * 256),
                     static_cast<uint8_t>(std::clamp(pew::sqrt(v[2]), (T)0.0, (T)0.999) * 256) };
        }

        inline static NaiveVector
        random() {
            return NaiveVector { random_type::randomDouble(),
                                 random_type::randomDouble(),
                                 random_type::randomDouble() };
        }

        inline static NaiveVector
        random(T min, T max) {
            return NaiveVector { random_type::randomDouble(min, max),
                                 random_type::randomDouble(min, max),
                                 random_type::randomDouble(min, max) };
        }

        inline static NaiveVector
        randomInUnitSphere() {
            while (true) {
                auto p = random(-1, 1);
                if (p.squareLength() >= 1) continue;
                return p;
            }
        }

        inline static NaiveVector
        randomInUnitDisk() {
            while (true) {
                auto p = NaiveVector(
                    random_type::randomDouble(-1, 1), random_type::randomDouble(-1, 1), 0);
                if (p.squareLength() >= 1) continue;
                return p;
            }
        }

        inline bool
        nearZero() const {
            constexpr auto s = 1e-8;
            return (fabs(v[0]) < s) && (fabs(v[1]) < s) && (fabs(v[2]) < s);
        }

        inline NaiveVector
        reflect(const NaiveVector<T>& normal) const {
            return *this - 2 * dot(normal) * normal;
        }

        inline NaiveVector
        refract(const NaiveVector& normal,
                double             etaiOverEtat,
                const double*      cosThetaRet = nullptr) const {
            double cosTheta;
            if (cosThetaRet)
                cosTheta = *cosThetaRet;
            else
                cosTheta = fmin(dot(-normal), 1.0);
            const auto rOutPerp     = etaiOverEtat * (*this + cosTheta * normal);
            const auto rOutParallel = -sqrt(fabs(1.0 - rOutPerp.squareLength())) * normal;
            return rOutPerp + rOutParallel;
        }
    };

    template<typename T>
    constexpr NaiveVector<T>
    operator*(const T& s, const NaiveVector<T>& v) {
        return v * s;
    }

    template<typename T>
    inline std::ostream&
    operator<<(std::ostream& os, const NaiveVector<T>& vec) {
        return os << "( " << vec.x() << " " << vec.y() << " " << vec.z() << " )";
    }

} // namespace pew

namespace std {
    template<>
    struct hash<pew::NaiveVector<double>> {
        size_t
        operator()(pew::NaiveVector<double> const& v) const {
            size_t            seed = 0;
            std::hash<double> hasher;

            auto hash_combine = [&](size_t hash) {
                hash += 0x9e3779b9 + (seed << 6) + (seed >> 2);
                seed ^= hash;
            };

            hash_combine(v.x());
            hash_combine(v.y());
            hash_combine(v.z());

            return seed;
        }
    };
} // namespace std

#endif
