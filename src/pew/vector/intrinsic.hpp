#ifndef PEW_INTRINSIC
#define PEW_INTRINSIC

#include <emmintrin.h>
#include <immintrin.h>
#include <smmintrin.h>
#include <tmmintrin.h>
#include <xmmintrin.h>

namespace pew {
    template<typename T>
    struct intrinsic;

    template<>
    struct intrinsic<double> {
        using type                    = __m256d;
        static const size_t alignment = 32;

        inline static auto
        set(double x, double y, double z) {
            return _mm256_set_pd(0, z, y, x);
        }

        inline static auto
        set(double c) {
            return _mm256_set1_pd(c);
        }

        inline static auto
        store(double* ptr, type vec) {
            return _mm256_store_pd(ptr, vec);
        }

        inline static auto
        mul(const type& left, const type& right) {
            return _mm256_mul_pd(left, right);
        }

        inline static auto
        mul(const type& vec, double s) {
            return mul(vec, set(s));
        }

        inline static auto
        sign(const type& vec) {
            return mul(vec, set(-1.0));
        }

        inline static auto
        add(const type& left, const type& right) {
            return _mm256_add_pd(left, right);
        }

        inline static auto
        sub(const type& left, const type& right) {
            return _mm256_sub_pd(left, right);
        }

        inline static double
        dot(const type& left, const type& right) {
            alignas(alignment) double arr[4];
            store(arr, mul(left, right));
            return arr[0] + arr[1] + arr[2];
        }

        inline static auto
        div(const type& vec, double s) {
            return _mm256_div_pd(vec, set(s));
        }

        inline static auto
        abs(const type& abs) {
            alignas(alignment) double arr[4];
            store(arr, abs);
            return set(std::abs(arr[0]), std::abs(arr[1]), std::abs(arr[2]));
        }
    };

    template<>
    struct intrinsic<int> {
        using type                         = __m128i;
        static constexpr size_t alignment  = 8;
        static constexpr auto   store_mask = 16;

        inline static auto
        set(int x, int y, int z) {
            return _mm_set_epi32(0, z, y, x);
        }

        inline static auto
        set(int c) {
            return _mm_set1_epi32(c);
        }

        inline static auto
        store(int* ptr, const type& vec) {
            return _mm_store_epi32(ptr, vec);
        }

        inline static auto
        mul(const type& left, const type& right) {
            return _mm_mullo_epi32(left, right);
        }

        inline static auto
        mul(const type& vec, int s) {
            return mul(vec, set(s));
        }

        inline static auto
        sign(const type& vec) {
            return _mm_sign_epi32(vec, set(-1));
        }

        inline static auto
        add(const type& left, const type& right) {
            return _mm_add_epi32(left, right);
        }

        inline static auto
        sub(const type& left, const type& right) {
            return _mm_sub_epi32(left, right);
        }

        inline static int
        dot(const type& left, const type& right) {
            alignas(alignment) int arr[4];
            store(arr, mul(left, right));
            return arr[0] + arr[1] + arr[2];
        }

        inline static auto
        div(const type& vec, int s) {
            alignas(alignment) int arr[4];
            store(arr, vec);
            return set(arr[0] / s, arr[1] / s, arr[2] / s);
        }

        inline static auto
        abs(const type& abs) {
            return _mm_abs_epi32(abs);
        }
    };

    template<>
    struct intrinsic<float> {
        using type                    = __m128;
        static const size_t alignment = 32;

        inline static auto
        set(float x, float y, float z) {
            return _mm_set_ps(0, z, y, x);
        }

        inline static auto
        set(float c) {
            return _mm_set1_ps(c);
        }

        inline static auto
        store(float* ptr, type vec) {
            return _mm_store_ps(ptr, vec);
        }

        inline static auto
        mul(const type& left, const type& right) {
            return _mm_mul_ps(left, right);
        }

        inline static auto
        mul(const type& vec, float s) {
            return mul(vec, set(s));
        }

        inline static auto
        sign(const type& vec) {
            return mul(vec, set(-1.0));
        }

        inline static auto
        add(const type& left, const type& right) {
            return _mm_add_ps(left, right);
        }

        inline static auto
        sub(const type& left, const type& right) {
            return _mm_sub_ps(left, right);
        }

        inline static float
        dot(const type& left, const type& right) {
            alignas(alignment) float arr[4];
            store(arr, mul(left, right));
            return arr[0] + arr[1] + arr[2];
        }

        inline static auto
        div(const type& vec, float s) {
            return _mm_div_ps(vec, set(s));
        }

        inline static auto
        abs(const type& abs) {
            alignas(alignment) float arr[4];
            store(arr, abs);
            return set(std::abs(arr[0]), std::abs(arr[1]), std::abs(arr[2]));
        }
    };
} // namespace pew

#endif