#ifndef PEW_VECTOR_REFERENCE
#define PEW_VECTOR_REFERENCE

#include <pew/random/random.hpp>

#include <array>
#include <cmath>
#include <ostream>
#include <algorithm>

namespace pew {
    //==============================================================================================
    // Originally written in 2016 by Peter Shirley <ptrshrl@gmail.com>
    //
    // To the extent possible under law, the author(s) have dedicated all copyright and related and
    // neighboring rights to this software to the public domain worldwide. This software is
    // distributed without any warranty.
    //
    // You should have received a copy (see file COPYING.txt) of the CC0 Public Domain Dedication
    // along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
    //==============================================================================================

    using std::fabs;
    using std::sqrt;

    class RefVector {
        public:
        constexpr RefVector() : e { 0, 0, 0 } {}
        constexpr RefVector(double e0, double e1, double e2) : e { e0, e1, e2 } {}
        constexpr RefVector(double c) : e { c, c, c } {}

        double
        x() const {
            return e[0];
        }
        double
        y() const {
            return e[1];
        }
        double
        z() const {
            return e[2];
        }

        RefVector
        operator-() const {
            return RefVector(-e[0], -e[1], -e[2]);
        }
        double
        operator[](int i) const {
            return e[i];
        }
        double&
        operator[](int i) {
            return e[i];
        }

        RefVector&
        operator+=(const RefVector& v) {
            e[0] += v.e[0];
            e[1] += v.e[1];
            e[2] += v.e[2];
            return *this;
        }

        RefVector&
        operator*=(const double t) {
            e[0] *= t;
            e[1] *= t;
            e[2] *= t;
            return *this;
        }

        RefVector&
        operator/=(const double t) {
            return *this *= 1 / t;
        }

        double
        length() const {
            return sqrt(squareLength());
        }

        double
        squareLength() const {
            return e[0] * e[0] + e[1] * e[1] + e[2] * e[2];
        }

        bool
        nearZero() const {
            // Return true if the vector is close to zero in all dimensions.
            const auto s = 1e-8;
            return (fabs(e[0]) < s) && (fabs(e[1]) < s) && (fabs(e[2]) < s);
        }

        inline static RefVector
        random() {
            return RefVector(
                pew::PCG_T::randomDouble(), pew::PCG_T::randomDouble(), pew::PCG_T::randomDouble());
        }

        inline static RefVector
        random(double min, double max) {
            return RefVector(pew::PCG_T::randomDouble(min, max),
                             pew::PCG_T::randomDouble(min, max),
                             pew::PCG_T::randomDouble(min, max));
        }

        inline double
        dot(const RefVector& other) const {
            return e[0] * other.e[0] + e[1] * other.e[1] + e[2] * other.e[2];
        }

        static inline RefVector
        randomInUnitSphere() {
            while (true) {
                auto p = RefVector::random(-1, 1);
                if (p.squareLength() >= 1) continue;
                return p;
            }
        }

        inline RefVector
        operator+(const RefVector& other) const {
            return RefVector(e[0] + other.e[0], e[1] + other.e[1], e[2] + other.e[2]);
        }

        inline RefVector
        operator-(const RefVector& other) const {
            return RefVector(e[0] - other.e[0], e[1] - other.e[1], e[2] - other.e[2]);
        }

        inline RefVector
        operator*(double t) const {
            return RefVector(t * e[0], t * e[1], t * e[2]);
        }

        inline RefVector
        operator*(const RefVector& other) const {
            return RefVector(e[0] * other.e[0], e[1] * other.e[1], e[2] * other.e[2]);
        }

        inline RefVector
        operator/(double t) const {
            return *this * (1 / t);
        }

        inline RefVector
        unit() const {
            return *this / length();
        }

        inline RefVector
        reflect(const RefVector& normal) const {
            return *this - normal * 2 * dot(normal) ;
        }

        inline std::array<uint8_t, 3> asColor() const {
            return { static_cast<uint8_t>(std::clamp(pew::sqrt(e[0]), 0.0, 0.999) * 256),
                     static_cast<uint8_t>(std::clamp(pew::sqrt(e[1]), 0.0, 0.999) * 256),
                     static_cast<uint8_t>(std::clamp(pew::sqrt(e[2]), 0.0, 0.999) * 256) };
        }

        public:
        double e[3];
    };

    // RefVector Utility Functions

    inline std::ostream&
    operator<<(std::ostream& out, const RefVector& v) {
        return out << v.e[0] << ' ' << v.e[1] << ' ' << v.e[2];
    }

    inline RefVector
    operator*(double t, const RefVector& v) {
        return RefVector(t * v.e[0], t * v.e[1], t * v.e[2]);
    }

    inline double
    dot(const RefVector& u, const RefVector& v) {
        return u.e[0] * v.e[0] + u.e[1] * v.e[1] + u.e[2] * v.e[2];
    }

    inline RefVector
    cross(const RefVector& u, const RefVector& v) {
        return RefVector(u.e[1] * v.e[2] - u.e[2] * v.e[1],
                         u.e[2] * v.e[0] - u.e[0] * v.e[2],
                         u.e[0] * v.e[1] - u.e[1] * v.e[0]);
    }

    inline RefVector
    unit_vector(RefVector v) {
        return v / v.length();
    }

    inline RefVector
    random_in_unit_disk() {
        while (true) {
            auto p = RefVector(PCG_T::randomDouble(-1, 1), PCG_T::randomDouble(-1, 1), 0);
            if (p.squareLength() >= 1) continue;
            return p;
        }
    }

    inline RefVector
    random_in_unit_sphere() {
        while (true) {
            auto p = RefVector::random(-1, 1);
            if (p.squareLength() >= 1) continue;
            return p;
        }
    }

    inline RefVector
    random_unit_vector() {
        return unit_vector(random_in_unit_sphere());
    }

    inline RefVector
    random_in_hemisphere(const RefVector& normal) {
        RefVector in_unit_sphere = random_in_unit_sphere();
        if (dot(in_unit_sphere, normal) > 0.0) // In the same hemisphere as the normal
            return in_unit_sphere;
        else
            return -in_unit_sphere;
    }

    inline RefVector
    reflect(const RefVector& v, const RefVector& n) {
        return v - 2 * dot(v, n) * n;
    }

    inline RefVector
    refract(const RefVector& uv, const RefVector& n, double etai_over_etat) {
        auto      cos_theta      = fmin(dot(-uv, n), 1.0);
        RefVector r_out_perp     = etai_over_etat * (uv + cos_theta * n);
        RefVector r_out_parallel = -sqrt(fabs(1.0 - r_out_perp.squareLength())) * n;
        return r_out_perp + r_out_parallel;
    }

} // namespace pew

#endif