#ifndef PEW_VECTOR_VECTOR
#define PEW_VECTOR_VECTOR

#include <cmath>
#include <pew/random/random.hpp>
#include <pew/tools/sqrt.hpp>
#include <pew/vector/intrinsic.hpp>

#include <algorithm>
#include <array>
#include <ostream>
#include <type_traits>
#include <variant>

namespace pew {
    template<typename T>
    class Vector final {
        using dyn_type   = typename intrinsic<T>::type;
        using sta_random = PCG_T;
        using dyn_random = PCG_T;

        public:
        using color_type = std::array<uint8_t, 3>;

        private:
        std::variant<std::array<T, 3>, dyn_type> v;

        public:
        constexpr Vector(T x, T y, T z) {
            if (std::is_constant_evaluated()) {
                v = std::array { x, y, z };
            } else {
                v = intrinsic<T>::set(x, y, z);
            }
        }
        explicit constexpr Vector(T c) {
            if (std::is_constant_evaluated()) {
                v = std::array { c, c, c };
            } else {
                v = intrinsic<T>::set(c);
            }
        }
        constexpr Vector() {
            if (std::is_constant_evaluated()) {
                v = std::array<T, 3> { 0, 0, 0 };
            } else {
                v = intrinsic<T>::set(0);
            }
        }

        constexpr Vector(const Vector& other) {
            if (std::is_constant_evaluated()) {
                v = other.v;
            } else {
                if (std::holds_alternative<dyn_type>(other.v)) {
                    v = other.v;
                } else {
                    const auto& ref = std::get<std::array<T, 3>>(other.v);

                    v = intrinsic<T>::set(ref[0], ref[1], ref[2]);
                }
            }
        }

        constexpr Vector(Vector&& other) {
            if (std::is_constant_evaluated()) {
                v = std::move(other.v);
            } else {
                if (std::holds_alternative<dyn_type>(other.v)) {
                    v = std::move(other.v);
                } else {
                    const auto& ref = std::get<std::array<T, 3>>(other.v);

                    v = intrinsic<T>::set(ref[0], ref[1], ref[2]);
                }
            }
        }

        constexpr Vector&
        operator=(const Vector& other) {
            if (std::is_constant_evaluated()) {
                v = other.v;
                return *this;
            } else {
                if (std::holds_alternative<dyn_type>(other.v)) {
                    v = other.v;
                } else {
                    const auto& ref = std::get<std::array<T, 3>>(other.v);

                    v = intrinsic<T>::set(ref[0], ref[1], ref[2]);
                }
                return *this;
            }
        }

        constexpr Vector
        operator=(Vector&& other) {
            if (std::is_constant_evaluated()) {
                v = std::move(other.v);
                return *this;
            } else {
                if (std::holds_alternative<dyn_type>(other.v)) {
                    v = std::move(other.v);
                } else {
                    const auto& ref = std::get<std::array<T, 3>>(other.v);

                    v = intrinsic<T>::set(ref[0], ref[1], ref[2]);
                }
                return *this;
            }
        }

        private:
        Vector(const dyn_type& d) : v(d) {}

        public:
        constexpr std::array<T, 3>
        clone() const {
            if (std::is_constant_evaluated()) return std::get<std::array<T, 3>>(v);

            if (std::holds_alternative<dyn_type>(v)) {
                alignas(intrinsic<T>::alignment) T d[4];
                intrinsic<T>::store(d, std::get<dyn_type>(v));
                return { d[0], d[1], d[2] };
            }

            return std::get<std::array<T, 3>>(v);
        }

        inline dyn_type
        asRegister() const {
            if (std::holds_alternative<dyn_type>(v)) {
                return std::get<dyn_type>(v);
            } else {
                const auto& ref = std::get<std::array<T, 3>>(v);
                return intrinsic<T>::set(ref[0], ref[1], ref[2]);
            }
        }

        constexpr T
        x() const {
            return clone()[0];
        }
        constexpr T
        y() const {
            return clone()[1];
        }
        constexpr T
        z() const {
            return clone()[2];
        }

        bool
        operator==(const Vector<double>& other) const {
            if (std::is_constant_evaluated()) {
                const auto& aRef = std::get<std::array<T, 3>>(v);
                const auto& bRef = std::get<std::array<T, 3>>(other.v);
                return aRef[0] == bRef[0] && aRef[1] == bRef[1] && aRef[2] == bRef[2];
            } else {
                const auto aClone = clone();
                const auto bClone = other.clone();
                return aClone[0] == bClone[0] && aClone[1] == bClone[1] && aClone[2] == bClone[2];
            }
        }

        constexpr Vector
        operator-() const {
            if (std::is_constant_evaluated()) {
                const auto& vecArr = std::get<std::array<T, 3>>(v);
                return { -vecArr[0], -vecArr[1], -vecArr[2] };
            } else {
                return intrinsic<T>::sign(asRegister());
            }
        }

        constexpr Vector
        operator+(const Vector& other) const {
            if (std::is_constant_evaluated()) {
                const auto& aRef = std::get<std::array<T, 3>>(v);
                const auto& bRef = std::get<std::array<T, 3>>(other.v);
                return { aRef[0] + bRef[0], aRef[1] + bRef[1], aRef[2] + bRef[2] };
            } else {
                return intrinsic<T>::add(asRegister(), other.asRegister());
            }
        }

        constexpr Vector
        operator-(const Vector& other) const {
            if (std::is_constant_evaluated()) {
                const auto& aRef = std::get<std::array<T, 3>>(v);
                const auto& bRef = std::get<std::array<T, 3>>(other.v);
                return { aRef[0] - bRef[0], aRef[1] - bRef[1], aRef[2] - bRef[2] };
            } else {
                return intrinsic<T>::sub(asRegister(), other.asRegister());
            }
        }

        constexpr Vector
        operator+=(const Vector& other) {
            if (std::is_constant_evaluated()) {
                auto& aRef = std::get<std::array<T, 3>>(v);
                auto& bRef = std::get<std::array<T, 3>>(other.v);
                aRef[0] += bRef[0];
                aRef[1] += bRef[1];
                aRef[2] += bRef[2];
                return *this;
            } else {
                v = intrinsic<T>::add(asRegister(), other.asRegister());
                return *this;
            }
        }

        constexpr Vector
        operator-=(const Vector& other) {
            if (std::is_constant_evaluated()) {
                *this += -other;
                return *this;
            } else {
                v = intrinsic<T>::sub(asRegister(), other.asRegister());
                return *this;
            }
        }

        constexpr Vector
        operator*(const T& s) const {
            if (std::is_constant_evaluated()) {
                const auto& vecArr = std::get<std::array<T, 3>>(v);
                return { vecArr[0] * s, vecArr[1] * s, vecArr[2] * s };
            } else {
                return intrinsic<T>::mul(asRegister(), s);
            }
        }

        constexpr Vector&
        operator*=(const T& s) {
            if (std::is_constant_evaluated()) {
                auto& vecArr = std::get<std::array<T, 3>>(v);
                vecArr[0] *= s;
                vecArr[1] *= s;
                vecArr[2] *= s;
                return *this;
            } else {
                v = intrinsic<T>::mul(asRegister(), s);
                return *this;
            }
        }

        constexpr Vector
        operator*(const Vector& other) const {
            if (std::is_constant_evaluated()) {
                const auto& aRef = std::get<std::array<T, 3>>(v);
                const auto& bRef = std::get<std::array<T, 3>>(other.v);
                return { aRef[0] * bRef[0], aRef[1] * bRef[1], aRef[2] * bRef[2] };
            } else {
                return intrinsic<T>::mul(asRegister(), other.asRegister());
            }
        }

        constexpr T
        dot(const Vector& other) const {
            if (std::is_constant_evaluated()) {
                const auto& aRef = std::get<std::array<T, 3>>(v);
                const auto& bRef = std::get<std::array<T, 3>>(other.v);
                return aRef[0] * bRef[0] + aRef[1] * bRef[1] + aRef[2] * bRef[2];
            } else {
                return intrinsic<T>::dot(asRegister(), other.asRegister());
            }
        }

        constexpr Vector
        cross(const Vector& other) const {
            auto a = clone();
            auto b = other.clone();

            // TODO: optimize for intrinsics
            return { a[1] * b[2] - a[2] * b[1],
                     a[2] * b[0] - a[0] * b[2],
                     a[0] * b[1] - a[1] * b[0] };
        }

        constexpr Vector
        operator/(const T& s) const {
            if (std::is_constant_evaluated()) {
                const auto& vecArr = std::get<std::array<T, 3>>(v);
                return { vecArr[0] / s, vecArr[1] / s, vecArr[2] / s };
            } else {
                return intrinsic<T>::div(asRegister(), s);
            }
        }

        constexpr Vector&
        operator/=(const T& s) {
            if (std::is_constant_evaluated()) {
                auto& vecArr = std::get<std::array<T, 3>>(v);
                vecArr[0] /= s;
                vecArr[1] /= s;
                vecArr[2] /= s;
                return *this;
            } else {
                v = intrinsic<T>::div(asRegister(), s);
                return *this;
            }
        }

        constexpr T
        squareLength() const {
            return dot(*this);
        }

        constexpr T
        length() const {
            if (std::is_constant_evaluated()) {
                return pew::sqrt(squareLength());
            } else {
                return std::sqrt(squareLength());
            }
        }

        constexpr Vector
        unit() const {
            return *this / this->length();
        }

        constexpr Vector
        abs() const {
            if (std::is_constant_evaluated()) {
                auto& vecArr = std::get<std::array<T, 3>>(v);
                return { std::abs(vecArr[0]), std::abs(vecArr[1]), std::abs(vecArr[2]) };
            } else {
                auto vecArr = clone();
                return intrinsic<T>::abs(asRegister());
            }
        }

        constexpr T
        max() const {
            if (std::is_constant_evaluated()) {
                auto& vecArr = std::get<std::array<T, 3>>(v);
                return std::max(vecArr[0], std::max(vecArr[1], vecArr[2]));
            } else {
                auto vecArr = clone();
                return std::max(vecArr[0], std::max(vecArr[1], vecArr[2]));
            }
        }

        constexpr T
        acos(const Vector& other) const {
            return std::acos((*this * other) / std::sqrt(squareLength() * other.squareLength()));
        }

        constexpr color_type
        asColor() const {
            if (std::is_constant_evaluated()) {
                const auto& vecArr = std::get<std::array<T, 3>>(v);
                if constexpr (std::is_floating_point_v<T>) {
#if defined(PEW_DISABLE_GAMMA_CORRECTION)
                    return { static_cast<uint8_t>(std::clamp(vecArr[0], (T)0.0, (T)0.999) * 256),
                             static_cast<uint8_t>(std::clamp(vecArr[1], (T)0.0, (T)0.999) * 256),
                             static_cast<uint8_t>(std::clamp(vecArr[2], (T)0.0, (T)0.999) * 256) };
#endif
                    return { static_cast<uint8_t>(std::clamp(pew::sqrt(vecArr[0]), (T)0.0, (T)0.999)
                                                  * 256),
                             static_cast<uint8_t>(std::clamp(pew::sqrt(vecArr[1]), (T)0.0, (T)0.999)
                                                  * 256),
                             static_cast<uint8_t>(std::clamp(pew::sqrt(vecArr[2]), (T)0.0, (T)0.999)
                                                  * 256) };
                } else {
                    return { static_cast<uint8_t>(std::clamp(vecArr[0], 0, 255)),
                             static_cast<uint8_t>(std::clamp(vecArr[1], 0, 255)),
                             static_cast<uint8_t>(std::clamp(vecArr[2], 0, 255)) };
                }
            } else {
                auto vecArr = clone();
                if constexpr (std::is_floating_point_v<T>) {
#if defined(PEW_DISABLE_GAMMA_CORRECTION)
                    return { static_cast<uint8_t>(std::clamp(vecArr[0], (T)0.0, (T)0.999) * 256),
                             static_cast<uint8_t>(std::clamp(vecArr[1], (T)0.0, (T)0.999) * 256),
                             static_cast<uint8_t>(std::clamp(vecArr[2], (T)0.0, (T)0.999) * 256) };
#endif
                    return { static_cast<uint8_t>(std::clamp(std::sqrt(vecArr[0]), (T)0.0, (T)0.999)
                                                  * 256),
                             static_cast<uint8_t>(std::clamp(std::sqrt(vecArr[1]), (T)0.0, (T)0.999)
                                                  * 256),
                             static_cast<uint8_t>(std::clamp(std::sqrt(vecArr[2]), (T)0.0, (T)0.999)
                                                  * 256) };

                } else {
                    return { static_cast<uint8_t>(std::clamp(vecArr[0], 0, 255)),
                             static_cast<uint8_t>(std::clamp(vecArr[1], 0, 255)),
                             static_cast<uint8_t>(std::clamp(vecArr[2], 0, 255)) };
                }
            }
        }

        inline static Vector
        random() {
            if constexpr (std::is_floating_point_v<T>) {
                return Vector { dyn_random::randomDouble(),
                                dyn_random::randomDouble(),
                                dyn_random::randomDouble() };
            } else {
                return Vector { dyn_random::random(), dyn_random::random(), dyn_random::random() };
            }
        }

        inline static Vector
        random(T min, T max) {
            if constexpr (std::is_floating_point_v<T>) {
                return Vector { dyn_random::randomDouble(min, max),
                                dyn_random::randomDouble(min, max),
                                dyn_random::randomDouble(min, max) };
            } else {
                return Vector { dyn_random::random(min, max),
                                dyn_random::random(min, max),
                                dyn_random::random(min, max) };
            }
        }

        inline static Vector
        randomInUnitSphere() {
            if constexpr (std::is_floating_point_v<T>) {
                while (true) {
                    auto p = random(-1.0, 1.0);
                    if (p.squareLength() >= 1) continue;
                    return p;
                }
            } else {
                return Vector {};
            }
        }

        inline static Vector
        randomInUnitDisk() {
            while (true) {
                auto p
                    = Vector(dyn_random::randomDouble(-1, 1), dyn_random::randomDouble(-1, 1), 0);
                if (p.squareLength() >= 1) continue;
                return p;
            }
        }

        constexpr bool
        nearZero() const {
            constexpr auto s = 1e-8;
            if (std::is_constant_evaluated()) {
                const auto& vecArr = std::get<std::array<T, 3>>(v);
                return (fabs(vecArr[0]) < s) && (fabs(vecArr[1]) < s) && (fabs(vecArr[2]) < s);
            } else {
                auto vecArr = clone();
                return (fabs(vecArr[0]) < s) && (fabs(vecArr[1]) < s) && (fabs(vecArr[2]) < s);
            }
        }

        constexpr Vector
        reflect(const Vector& normal) const {
            return *this - normal * 2 * dot(normal);
        }

        constexpr Vector
        refract(const Vector& normal,
                double        etaiOverEtat,
                const double* cosThetaRet = nullptr) const {
            double cosTheta;
            if (cosThetaRet)
                cosTheta = *cosThetaRet;
            else
                cosTheta = fmin(dot(-normal), 1.0);
            const auto rOutPerp     = etaiOverEtat * (*this + cosTheta * normal);
            const auto rOutParallel = -sqrt(fabs(1.0 - rOutPerp.squareLength())) * normal;
            return rOutPerp + rOutParallel;
        }
    };

    template<typename T>
    constexpr Vector<T>
    operator*(const T& s, const Vector<T>& v) {
        return v * s;
    }

    template<typename T>
    inline std::ostream&
    operator<<(std::ostream& os, const Vector<T>& vec) {
        const auto data = vec.clone();
        return os << "( " << data[0] << " " << data[1] << " " << data[2] << " )";
    }

} // namespace pew

namespace std {
    template<>
    struct hash<pew::Vector<double>> {
        size_t
        operator()(pew::Vector<double> const& v) const {
            size_t            seed = 0;
            std::hash<double> hasher;
            auto              clone = v.clone();

            auto hash_combine = [&](size_t hash) {
                hash += 0x9e3779b9 + (seed << 6) + (seed >> 2);
                seed ^= hash;
            };

            hash_combine(clone[0]);
            hash_combine(clone[1]);
            hash_combine(clone[2]);

            return seed;
        }
    };
} // namespace std

#endif