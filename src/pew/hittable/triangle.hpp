#ifndef PEW_HITTABLE_TRIANGLE
#define PEW_HITTABLE_TRIANGLE

#include <pew/hittable/hittable.hpp>
#include <pew/material/material.hpp>
#include <pew/vector/common.hpp>
#include <pew/vertex/vertex.hpp>

namespace pew {
    struct Triangle final : public Hittable {
        Vertex v0;
        Vertex v1;
        Vertex v2;

        Triangle() = default;
        Triangle(const Vertex&             a,
                 const Vertex&             b,
                 const Vertex&             c,
                 std::shared_ptr<Material> material) :
            Hittable(material),
            v0(a), v1(b), v2(c) {}

        std::optional<Hit>
        hit(const Ray& ray) const override {
            auto e1    = v1.point - v0.point;
            auto e2    = v2.point - v0.point;
            auto ni1   = v1.normal - v0.normal;
            auto ni2   = v2.normal - v0.normal;
            auto texi1 = tex2 { v1.texPos[0] - v0.texPos[0], v1.texPos[1] - v0.texPos[1] };
            auto texi2 = tex2 { v2.texPos[0] - v0.texPos[0], v2.texPos[1] - v0.texPos[1] };

            auto h = ray.getDirection().cross(e2);
            auto a = e1.dot(h);

            if (a > -0.00001 && a < 0.00001) return std::nullopt;

            auto f = 1 / a;
            auto s = ray.getOrigin() - v0.point;
            auto u = f * s.dot(h);

            if (u < 0.0 || u > 1.0) return std::nullopt;

            auto q = s.cross(e1);
            auto v = f * ray.getDirection().dot(q);

            if (v < 0.0 || u + v > 1.0) return std::nullopt;

            auto t = f * e2.dot(q);

            if (t < ray.getRange().first || t > ray.getRange().second) return std::nullopt;

            Hit hit;
            hit.t        = t;
            hit.ray      = ray;
            hit.material = material;

            hit.tex = tex2 {
                v0.texPos[0] + texi1[0] * u + texi2[0] * v,
                v0.texPos[1] + texi1[1] * u + texi2[1] * v,
            };

            auto normal = (v0.normal + ni1 * u + ni2 * v).unit();
            hit.setFaceNormal(normal);
            // hit.setFaceNormal(e1.cross(e2).unit());

            return hit;
        }
    };
} // namespace pew

#endif