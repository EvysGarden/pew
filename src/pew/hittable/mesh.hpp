#ifndef PEW_HITTABLE_MESH
#define PEW_HITTABLE_MESH

#include <pew/hittable/collection.hpp>
#include <pew/vertex/vertex.hpp>

#include <filesystem>
#include <vector>

namespace pew {

    class Mesh final {
        vec3                     origin;
        std::vector<Vertex>      vertices;
        std::vector<std::size_t> indices;

        public:
        Mesh(const std::filesystem::path& obj, const vec3& origin);

        Collection
        collect(std::shared_ptr<Material> material) const;
    };
} // namespace pew

#endif