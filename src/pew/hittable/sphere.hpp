#ifndef PEW_HITTABLE_SPHERE
#define PEW_HITTABLE_SPHERE

#include <memory>
#include <pew/hittable/hittable.hpp>
#include <pew/material/material.hpp>
#include <pew/ray/ray.hpp>
#include <pew/tools/sqrt.hpp>
#include <pew/vector/common.hpp>

#include <optional>
#include <type_traits>

namespace pew {
    struct Sphere final : public Hittable {
        vec3   center;
        double radius;

        public:
        Sphere() = default;
        Sphere(const vec3& center, double radius, std::shared_ptr<Material> material) :
            center(center), radius(radius), Hittable(material) {}
        Sphere(const vec3& center, double radius) : center(center), radius(radius) {}

        std::optional<Hit>
        hit(const Ray& ray) const override {
            vec3 oc     = ray.getOrigin() - center;
            auto a      = ray.getDirection().squareLength();
            auto half_b = oc.dot(ray.getDirection());
            auto c      = oc.squareLength() - radius * radius;

            auto discriminant = half_b * half_b - a * c;
            if (discriminant < 0) return std::nullopt;
            auto sqrtd = sqrt(discriminant);

            // Find the nearest root that lies in the acceptable range.
            auto root = (-half_b - sqrtd) / a;
            if (root < ray.getRange().first || ray.getRange().second < root) {
                root = (-half_b + sqrtd) / a;
                if (root < ray.getRange().first || ray.getRange().second < root)
                    return std::nullopt;
            }

            Hit hit;
            hit.t               = root;
            hit.ray             = ray;
            vec3 outward_normal = (hit.getPoint() - center) / radius;
            hit.setFaceNormal(outward_normal);
            hit.material = material;

            return hit;
        }
    };
} // namespace pew

#endif