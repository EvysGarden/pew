#include "hittable.hpp"
#include <pew/material/material.hpp>

namespace pew {
    std::optional<Ray>
    Hit::scatter(vec3& attenuation) const {
        if (material) return material->scatter(*this, attenuation);
        return std::nullopt;
    }
}