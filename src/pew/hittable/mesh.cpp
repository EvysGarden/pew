#include "mesh.hpp"

#include <pew/hittable/collection.hpp>
#include <pew/hittable/triangle.hpp>
#include <pew/material/normal.hpp>

#include <memory>
#include <stdexcept>
#include <unordered_map>
#include <vector>

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader/tiny_obj_loader.h>

namespace fs = std::filesystem;

namespace pew {
    Mesh::Mesh(const fs::path& obj, const vec3& origin) : origin(origin) {
        tinyobj::attrib_t                attrib;
        std::vector<tinyobj::shape_t>    shapes;
        std::vector<tinyobj::material_t> materials;
        std::string                      warn, err;

        if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, obj.c_str())) {
            throw std::runtime_error(warn + err);
        }

        std::unordered_map<Vertex, std::size_t> uniqueVertices {};

        for (const auto& shape : shapes) {
            for (const auto& index : shape.mesh.indices) {
                Vertex vertex;

                vertex.point = origin
                               + vec3(attrib.vertices[3 * index.vertex_index + 1],
                                      attrib.vertices[3 * index.vertex_index + 2],
                                      attrib.vertices[3 * index.vertex_index + 0]);

                vertex.normal = vec3(attrib.normals[3 * index.normal_index + 1],
                                     attrib.normals[3 * index.normal_index + 2],
                                     attrib.normals[3 * index.normal_index + 0])
                                    .unit();

                vertex.texPos = tex2 { attrib.texcoords[2 * index.texcoord_index + 0],
                                       attrib.texcoords[2 * index.texcoord_index + 1] };

                if (!uniqueVertices.contains(vertex)) {
                    uniqueVertices[vertex] = vertices.size();
                    vertices.push_back(vertex);
                }

                indices.push_back(uniqueVertices[vertex]);
            }
        }
    }

    Collection
    Mesh::collect(std::shared_ptr<Material> material) const {
        Collection collection;

        for (size_t i = 0; i < indices.size(); i += 3) {
            collection.add<Triangle>(
                vertices[indices[i]], vertices[indices[i + 1]], vertices[indices[i + 2]], material);
        }

        return collection;
    }
} // namespace pew