#ifndef PEW_HITTABLE_COLLECTION
#define PEW_HITTABLE_COLLECTION

#include <memory>
#include <pew/hittable/hittable.hpp>
#include <pew/vector/common.hpp>
#include <utility>
#include <vector>

namespace pew {
    struct Collection final : public Hittable {
        std::vector<std::shared_ptr<Hittable>> collection;

        Collection() = default;

        std::optional<Hit>
        hit(const Ray& ray) const override {
            std::optional<Hit> closest = std::nullopt;
            Ray                rayCopy { ray };

            for (const auto& hittable : collection) {
                if (auto result = hittable->hit(rayCopy)) {
                    if (!closest || closest->t > result->t) {
                        closest = result;
                        rayCopy.setRange({ rayCopy.getRange().first, result->t });
                    }
                }
            }

            return closest;
        }

        template<typename T, typename... Args>
        auto&
        add(Args&&... args) {
            return collection.emplace_back(new T(std::forward<Args>(args)...));
        }
    };
} // namespace pew

#endif