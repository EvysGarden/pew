#ifndef PEW_HITTABLE_HITTABLE
#define PEW_HITTABLE_HITTABLE

#include <memory>
#include <pew/ray/ray.hpp>
#include <pew/vector/common.hpp>
#include <pew/vertex/vertex.hpp>

#include <optional>

namespace pew {
    class Hittable;
    class Hit;
    class Material;
    class World;

    typedef enum HitConfigFlagBits :
        uint8_t
    {
        eHitConfigPureOpaque = 1
    } HitConfigFlagBits;
    typedef uint8_t HitConfigFlags;

    class Hittable {
        protected:
        std::shared_ptr<Material> material;

        public:
        Hittable() = default;
        Hittable(std::shared_ptr<Material> material) : material(material) {}

        virtual std::optional<Hit>
        hit(const Ray& ray) const = 0;

        void
        setMaterial(std::shared_ptr<Material> material) {
            this->material = material;
        }

        std::weak_ptr<Material>
        getMaterial() {
            return material;
        }

        virtual ~Hittable() {}
    };

    struct Hit {
        Ray                       ray;
        double                    t;
        std::shared_ptr<Material> material;
        bool                      frontFace = false;
        vec3                      normal {};
        tex2                      tex;

        vec3
        getPoint() const {
            return ray(t);
        }

        operator bool() const { return !!material; }
        auto&
        operator->() const {
            return material;
        }

        void
        setFaceNormal(const vec3& normal) {
            this->frontFace = ray.getDirection().dot(normal) < 0;
            this->normal    = frontFace ? normal : -normal;
        }

        std::optional<Ray>
        scatter(vec3& attenuation) const;
    };
} // namespace pew

#endif