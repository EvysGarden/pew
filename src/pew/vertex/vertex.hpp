#ifndef PEW_VERTEX_VERTEX
#define PEW_VERTEX_VERTEX

#include <pew/vector/common.hpp>

namespace pew {
    using tex2 = std::array<double, 2>;

    struct Vertex final {
        vec3 point;
        vec3 normal;
        tex2 texPos;

        bool
        operator==(const Vertex& other) const {
            return point == other.point && normal == other.normal && texPos == other.texPos;
        }
    };
} // namespace pew

namespace std {
    template<>
    struct hash<pew::tex2> {
        size_t
        operator()(pew::tex2 const& tex) const {
            return hash<double>()(tex[0]) ^ hash<double>()(tex[1]) << 1;
        }
    };

    template<>
    struct hash<pew::Vertex> {
        size_t
        operator()(pew::Vertex const& vertex) const {
            return ((hash<pew::vec3>()(vertex.point) ^ hash<pew::vec3>()(vertex.normal) << 1) >> 1)
                   ^ (hash<pew::tex2>()(vertex.texPos) << 1);
        }
    };
} // namespace std

#endif